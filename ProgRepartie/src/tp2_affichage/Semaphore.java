package tp2_affichage;

public abstract class Semaphore {

    protected int valeur=0;

    protected Semaphore (int valeurInitiale) {
    	valeur = valeurInitiale>0 ? valeurInitiale:0;
    }

    public synchronized void syncWait() { //On bloque un verou
		try {
		    while(valeur<=0){ // tant que = 0, on attends
		    	wait();
	        }
		    valeur--;
		} catch(InterruptedException e){}
    }

    public synchronized void syncSignal() { //On lib�re un verou
    	if(++valeur > 0) notifyAll();
    }
}
