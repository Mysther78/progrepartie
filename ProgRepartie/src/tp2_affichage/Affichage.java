package tp2_affichage;

import java.lang.String;

public class Affichage extends Thread {
	private String texte; 
	private static Semaphore verou = new SemaphoreBinaire(1);; 

	public Affichage (String txt){texte=txt;}
	
	public void run(){
		verou.syncWait();
		for (int i=0; i<texte.length(); i++){
	    	System.out.print(texte.charAt(i));
	    	try {sleep(100);} catch(InterruptedException e){};
		}
		verou.syncSignal();
		
		//Ou bien on fait un synchronized pour toute la boucle avec le m�me objet
	}
}
