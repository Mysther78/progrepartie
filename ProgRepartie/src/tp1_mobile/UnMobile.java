package tp1_mobile;
import java.awt.*;

import javax.swing.*;


class UnMobile extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	private static SemaphoreGeneral semaphore = new SemaphoreGeneral(3);
	
	int saLargeur, saHauteur, sonDebDessin, sonTemps;
    final int sonPas = 10, sonCote=40;
    
    UnMobile(int telleLargeur, int telleHauteur, int telleTemps) {
		super();
		saLargeur = telleLargeur;
		saHauteur = telleHauteur;
		sonTemps = telleTemps;
		setSize(telleLargeur, telleHauteur);
    }

    public void run() {
    	while(true) {
    		for (sonDebDessin=0; sonDebDessin < (saLargeur/3) - sonCote - sonPas ; sonDebDessin+= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    		semaphore.syncWait();
    		for (; sonDebDessin < (saLargeur/3)*2 - sonCote - sonPas ; sonDebDessin+= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    		semaphore.syncSignal();
    		for (; sonDebDessin < saLargeur - sonCote - sonPas ; sonDebDessin+= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    		
    		//retour
    		for (; sonDebDessin > (saLargeur/3)*2 ; sonDebDessin-= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    		
    		semaphore.syncWait();
    		for (; sonDebDessin > (saLargeur/3) ; sonDebDessin-= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    		semaphore.syncSignal();
    		
    		for (; sonDebDessin > 0 ; sonDebDessin-= sonPas) {
				repaint();
				try {Thread.sleep(sonTemps);} 
				catch (InterruptedException telleExcp) {
					telleExcp.printStackTrace();}
    		}
    	}
    }

    public void paintComponent(Graphics telCG) {
		super.paintComponent(telCG);
		Color color = new Color((int)(Math.random()*250),(int)(Math.random()*250),(int)(Math.random()*250));
		telCG.setColor(color);
		telCG.fillRect(sonDebDessin, (saHauteur - sonCote) / 2, sonCote, sonCote);
    }
}