package tp1_mobile;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

class UneFenetre extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private int nombreMobile = 10;
	private UnMobile[] sonMobile = new UnMobile[nombreMobile];
	private Thread[] maTache = new Thread[nombreMobile];
	private JButton[] button_StartStop = new JButton[nombreMobile];
	private boolean[] isActive = new boolean[nombreMobile];
    private final int LARG=1920/2, HAUT=60;
    
    public UneFenetre() {
    	super("Tp Mobile - Danoffre");
    	this.setSize(new Dimension(LARG, HAUT*nombreMobile));
    	this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    	this.setLayout(new GridLayout(0, 2));
    	this.setVisible(true);
    	
    	for(int i = 0 ; i < nombreMobile ; i++) {
        	//sonMobile[i] = new UnMobile(LARG/2, HAUT, 10+(int)(Math.random()*100));
        	sonMobile[i] = new UnMobile(LARG/2, HAUT, 60);
        	this.add(sonMobile[i]);
        	
        	button_StartStop[i] = new JButton("Start/Stop");
        	button_StartStop[i].addActionListener(this);
        	this.add(button_StartStop[i]);
        	
        	maTache[i] = new Thread(sonMobile[i]);
        	maTache[i].start();
        	isActive[i] = true;
    	}
    	
    	this.validate();
    	
	// TODO 
	// ajouter sonMobile a la fenetre
	// creer une thread laThread avec sonMobile
	// afficher la fenetre
	// lancer laThread 
    }

	public void actionPerformed(ActionEvent event) {
		for(int i = 0 ; i < nombreMobile ; i++) {
			if(event.getSource().equals(button_StartStop[i])) {
				if(isActive[i] == true){
					maTache[i].suspend();
					isActive[i] = false;
				} else {
					maTache[i].resume();
					isActive[i] = true;
				}
			}
		}
		
		
	}
}
