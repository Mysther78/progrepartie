package socketv2;

import java.io.*;
import java.net.*;

public class SocketServeur_Worker {
   static int port = 25545;

private static boolean isRunning = true;
   
   
   public static void main(String[] args) throws Exception {
        ServerSocket s = new ServerSocket(port);
        System.out.println("Serveur : " + "Serveur Started on " + port);
        Socket soc = s.accept();

        // Un BufferedReader permet de lire par ligne.
        BufferedReader plec = new BufferedReader(
                               new InputStreamReader(soc.getInputStream())
                              );

        // Un PrintWriter poss�de toutes les op�rations print classiques.
        // En mode auto-flush, le tampon est vid� (flush) � l'appel de println.
        PrintWriter pred = new PrintWriter(
                             new BufferedWriter(
                                new OutputStreamWriter(soc.getOutputStream())), 
                             true);

        while (isRunning) {
           String str = plec.readLine();          // lecture du message
           if (str.equals("END")) isRunning=false;
           System.out.println("Serveur recept : " +  str);   // trace locale
           pred.println(str + " recepted");                     // renvoi d'un �cho
        }
        plec.close();
        pred.close();
        soc.close();
   }
}