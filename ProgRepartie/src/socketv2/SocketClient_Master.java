package socketv2;

import java.io.*;
import java.net.*;
/** Le processus client se connecte au site fourni dans la commande
 *   d'appel en premier argument et utilise le port distant 8080.
 */
public class SocketClient_Master {
	static int n_serveur = 5;
	static final int[] tab_port = {25545,25546,25547,25548,25549,25550,25551};
	static String[] tab_n_cible = new String[n_serveur];
	static final String ip = "127.0.0.1";
	static BufferedReader[] reader = new BufferedReader[n_serveur];
	static PrintWriter[] writer = new PrintWriter[n_serveur];
	static Socket[] sockets = new Socket[n_serveur];
   

   public static void main(String[] args) throws Exception {

       int n_total = 16000000;
	   int n_cible = 0;
	   long startTime = System.currentTimeMillis();
	   
       //Creation d'un client
	   for(int i = 0 ; i < n_serveur ; i++) {
		    sockets[i] = new Socket(ip, tab_port[i]);
	        System.out.println("SOCKET = " + sockets[i]);

	        reader[i] = new BufferedReader(
	                               new InputStreamReader(sockets[i].getInputStream())
	                               );

	        writer[i] = new PrintWriter(
	                             new BufferedWriter(
	                                new OutputStreamWriter(sockets[i].getOutputStream())),
	                             true);

	        String message_to_send;
	        
           message_to_send = String.valueOf(n_total);
           
           writer[i].println(message_to_send);          // envoi d'un message
	   }
	   
	   //fait la demande de rep au client
	   for(int i = 0 ; i < n_serveur ; i++) {
	       tab_n_cible[i] = reader[i].readLine();      // lecture de l'�cho
	       System.out.println("Client echo : " + tab_n_cible[i]);
	       
	       System.out.println("END");     // message de terminaison
	       writer[i].println("END") ;
	       reader[i].close();
	       writer[i].close();
	       sockets[i].close();
	   }
	   
	   //Calcul de PI
	   for(int i = 0 ; i < n_serveur ; i++) {
		   n_cible+= Integer.parseInt(tab_n_cible[i]);
	   }
	   
	   long stopTime = System.currentTimeMillis();
	   System.out.println("Time Duration: " + (stopTime - startTime) + "ms");
	   System.out.println("PI = " + 4.0 * n_cible / n_total);
   }
}