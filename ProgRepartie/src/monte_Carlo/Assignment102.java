package monte_Carlo;

public class Assignment102 {
	
	public static void main(String[] args) {
		String s = "";
		for(int i = 0 ; i < 20 ; i++) {
			
			PiMonteCarlo PiVal = new PiMonteCarlo(16000000);
			long startTime = System.currentTimeMillis();
			double value = PiVal.getPi();
			long stopTime = System.currentTimeMillis();
			
			//System.out.println("Approx value:" + value);
			System.out.println(i + ": Difference to exact value of pi: " + (value - Math.PI));
			//System.out.println("Error: " + (value - Math.PI) / Math.PI * 100 + " %");
			//System.out.println("Available processors: " + Runtime.getRuntime().availableProcessors());
			System.out.println(i + ": Time Duration: " + (stopTime - startTime) + "ms");
			System.out.println();
			s += (stopTime - startTime) +  "\t";
			
		}
		System.out.println(s);
	}
}