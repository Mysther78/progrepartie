package monte_Carlo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Approximates PI using the Monte Carlo method.  Demonstrates
 * use of Callables, Futures, and thread pools.
 */
public class Pi 
{
  public static void main(String[] args) throws Exception 
  {
	String s = "";
	for(int i = 0 ; i < 20 ; i++) {
	    // 10 workers, 50000 iterations each
		long startTime = System.currentTimeMillis();
	    new Master().doRun(16000000, 9);
		long stopTime = System.currentTimeMillis();
		System.out.println("Time Duration: " + (stopTime - startTime) + "ms");
		System.out.println();
		s += (stopTime - startTime) +  "\t";
		}
	System.out.println(s);		
  }
}

/**
 * Creates workers to run the Monte Carlo simulation
 * and aggregates the results.
 */
class Master {
  public void doRun(int totalCount, int numWorkers) throws InterruptedException, ExecutionException 
  {
    // Create a collection of tasks
    List<Callable<Long>> tasks = new ArrayList<Callable<Long>>();
    for (int i = 0; i < numWorkers; ++i) 
    {
      tasks.add(new Worker(totalCount/numWorkers));
    }
    
    // Run them and receive a collection of Futures
    ExecutorService exec = Executors.newFixedThreadPool(numWorkers);
    List<Future<Long>> results = exec.invokeAll(tasks);
    long total = 0;
    
    // Assemble the results.
    for (Future<Long> f : results)
    {
      // Call to get() is an implicit barrier.  This will block
      // until result from corresponding worker is ready.
      total += f.get();
    }
    double pi = 4.0 * total / totalCount / numWorkers;
    System.out.println("Pi : " + pi);
    exec.shutdown();
  }
}