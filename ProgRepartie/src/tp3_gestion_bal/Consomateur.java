package tp3_gestion_bal;

public class Consomateur implements Runnable {
	char lettreLue;
	Boite_aux_lettres bal;
	
	public Consomateur(Boite_aux_lettres parBal) {
		this.bal = parBal;
	}

	public void run() {
		while(lettreLue != 'q') {
			lettreLue = bal.retirer();
			//System.out.println(" ---- La lettre '" + lettreLue + "' a �t� lu" );
			try {Thread.sleep(50);} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
