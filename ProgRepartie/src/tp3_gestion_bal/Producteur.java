package tp3_gestion_bal;

import java.util.Scanner;

public class Producteur implements Runnable {
	private boolean running = true;
	private Boite_aux_lettres bal;
	private Scanner reader;
	
	public Producteur(Boite_aux_lettres parBal) {
		this.bal = parBal;
	}

	public void run() {
		reader = new Scanner(System.in);
		while(running) {
			String line = reader.nextLine();
			
			for(int i = 0 ; i < line.length() ; i++) {
				bal.deposer(line.charAt(i));
				if(line.charAt(i) == 'q')
					running = false;
				try {Thread.sleep(30);} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			try {Thread.sleep(50);} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
