package tp3_gestion_bal.concurrent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Boite_aux_lettres {
	BlockingQueue<Character> buffer = new ArrayBlockingQueue<Character>(10);
	
	public boolean deposer(Character newLettre) throws InterruptedException {
		return buffer.offer(newLettre,  200, TimeUnit.MILLISECONDS) ;
	}
	
	public synchronized Character retirer() throws InterruptedException {
	       return buffer.poll(200, TimeUnit.MILLISECONDS) ;
	}

    public  int getStock() {
       return buffer.size() ;
   }
}
