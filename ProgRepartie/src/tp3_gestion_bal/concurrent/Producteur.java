package tp3_gestion_bal.concurrent;

import java.util.Scanner;

public class Producteur implements Runnable {
	private boolean running = true;
	private Boite_aux_lettres bal;
	private Scanner reader;
	
	public Producteur(Boite_aux_lettres parBal) {
		this.bal = parBal;
	}

	public void run() {
		reader = new Scanner(System.in);
		
		while(running) {
			try {
				Thread.sleep(200);
				String line = reader.nextLine();
				
				for(int i = 0 ; i < line.length() ; i++) {
					boolean added = bal.deposer(line.charAt(i));
					if(added) {
						System.out.println("Je livre '" + line.charAt(i) + "'.");
						if(line.charAt(i) == 'q') {
							System.out.println("Je m'arr�te.");
							running = false;
						}
					}
					else if(!added) {
						System.out.println("La boite est pleine.");	
						i--;
					}
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
