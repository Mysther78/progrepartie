package tp3_gestion_bal.concurrent;

public class Launch {
	public static void main(String[] args) {
		Boite_aux_lettres bal = new Boite_aux_lettres();
		Consomateur conso = new Consomateur(bal);
		Producteur produ = new Producteur(bal);

		Thread thConso = new Thread(conso);
		Thread thProdu = new Thread(produ);

		thConso.start();
		thProdu.start();
	}
}
