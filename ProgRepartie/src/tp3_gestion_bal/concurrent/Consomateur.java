package tp3_gestion_bal.concurrent;

public class Consomateur implements Runnable {
	Boite_aux_lettres bal;
	Character lettreLue;
	
	public Consomateur(Boite_aux_lettres parBal) {
		this.bal = parBal;
	}

	public void run() {
		
		try {
			do {
				Thread.sleep(1000);
				lettreLue = bal.retirer();
				if (lettreLue != null)
					System.out.println("Lettre '" + lettreLue + "' retir�e, il en reste " + bal.getStock());
			}
			while(lettreLue == null || lettreLue != 'q');
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
