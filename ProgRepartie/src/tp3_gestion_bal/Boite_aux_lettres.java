package tp3_gestion_bal;

import java.util.ArrayList;

public class Boite_aux_lettres {
	boolean isLettreAvailable = false;
	ArrayList<Character> buffer = new ArrayList<Character>();
	
	public Boite_aux_lettres() {}
	
	public synchronized void deposer(char newLettre) {
		while(buffer.size() > 10) {
			try { wait(); }
			catch(InterruptedException e) {
				System.out.println(e.toString());
			}
		}
		buffer.add(newLettre);
		//System.out.println("Nouvelle lettre ! '" + newLettre + "'");
		System.out.println("Buffer = " + buffer.toString() );
		isLettreAvailable = true;
		notifyAll();
	}
	
	public synchronized char retirer() {
		while(isLettreAvailable == false) {
			try { wait(); }
			catch(InterruptedException e) {
				e.printStackTrace();
			}			
		}
		char lettreEnvoyer = buffer.get(0);
		buffer.remove(0);
		System.out.println("Buffer = " + buffer.toString() );
		if(buffer.size() == 0)
			isLettreAvailable = false;
		notifyAll();
		return lettreEnvoyer;		
	}
}
